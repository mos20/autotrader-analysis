terraform {
  backend "gcs" {
    bucket = "footle-terraform-admin"
    prefix = "terraform/state"
  }
}
