import concurrent.futures
import datetime
import os

from autotrader_crawler.crawler import pull_page, parse_index_page, pull_details_page
from google.cloud import storage


def hello_world(request):
    storage_client = storage.Client()
    bucket_name = os.environ['STORAGE_BUCKET']
    bucket = storage_client.get_bucket(bucket_name)

    date = '{:%Y-%m-%d}'.format(datetime.datetime.now())

    page_number = 1
    while True:
        page = pull_page(page_number, 'SW12 8AH', 'PORSCHE', 'PANAMERA')
        page_root = date + '/' + str(page_number)
        blob = bucket.blob(page_root + '/index')
        blob.upload_from_string(page, 'application/json')

        index_page = parse_index_page(page)

        def pull_and_persist_advert(advert_id):
            advert_details = pull_details_page(advert_id)
            blob = bucket.blob(page_root + '/adverts/' + advert_id)
            blob.upload_from_string(advert_details, 'application/json')

        with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
            executor.map(pull_and_persist_advert, index_page.advert_ids)

        if not index_page.has_next_listing_page:
            break
        page_number = page_number + 1

    request_json = request.get_json()
    if request.args and 'message' in request.args:
        return request.args.get('message')
    elif request_json and 'message' in request_json:
        return request_json['message']
    else:
        return f'Hello World!'
