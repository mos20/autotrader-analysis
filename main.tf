provider "google" {
  version = "2.9.1"
  project = "autotrader-analysis-9134160"
  region = "europe-west2"
}

resource "google_project" "autotrader-analysis" {
  name = "Autotrader Analysis"
  project_id = "autotrader-analysis-9134160"
  org_id = "627980523512"
  billing_account = "01D3C5-807A3E-EE2305"
}

resource "google_project_services" "project" {
  project = google_project.autotrader-analysis.project_id
  services = [
    "appengine.googleapis.com",
    "cloudfunctions.googleapis.com",
    "cloudscheduler.googleapis.com",
    "cloudtrace.googleapis.com",
    "logging.googleapis.com",
    "pubsub.googleapis.com",
    "storage-api.googleapis.com",
    "storage-component.googleapis.com"]
}

data "archive_file" "function_source" {
  type = "zip"
  source_dir = "./function"
  output_path = "./index.zip"
}

resource "google_storage_bucket" "pulls" {
  project = google_project.autotrader-analysis.project_id
  name = "pulls9"
  depends_on = [
    google_project_services.project]
}

resource "google_storage_bucket" "bucket" {
  project = google_project.autotrader-analysis.project_id
  name = "my-function-bucket12"
  depends_on = [
    google_project_services.project]
}

resource "google_storage_bucket_object" "archive" {
  name = "${data.archive_file.function_source.output_md5}.zip"
  bucket = google_storage_bucket.bucket.name
  source = "./index.zip"
  depends_on = [
    data.archive_file.function_source]
}

resource "google_cloudfunctions_function" "function" {
  name = "function-test"
  description = "My function"
  runtime = "python37"

  available_memory_mb = 128
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http = true
  timeout = 540
  entry_point = "hello_world"
  environment_variables = {
    STORAGE_BUCKET = google_storage_bucket.pulls.name
  }
  depends_on = [
    google_project_services.project]
}

resource "google_app_engine_application" "app" {
  project = google_project.autotrader-analysis.project_id
  location_id = "europe-west2"
  depends_on = [
    google_project_services.project]
}

resource "google_cloud_scheduler_job" "job" {
  name = "autotrader-pull"
  description = "Autotrader pull job"
  schedule = "45 22 * * *"

  http_target {
    http_method = "GET"
    uri = google_cloudfunctions_function.function.https_trigger_url
  }
  depends_on = [
    google_project_services.project,
    google_app_engine_application.app]
}